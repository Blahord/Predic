# Predic
A simple static typed functional programming language

Note:
This project is just a "Let's see if i can" project. There is no intention for me to make a big or professional language from it.
Or to say it in Linux Torvald words: "It won't be big or professional".

Basic Syntax:

Each program consists of a set of assignments and one expression at the end:
```
let triag = function(Number n) {
    return n <= 0
        ? 0
        : n + this(n - 1);
};

return triag(5);
```
First we will assign triag to the given function, which takes a Number as parameter. (The return type inferd automatically. So you will not give it)
So triag is a function which takes one Number and returns a Number.
At the end triag is called with the Number 5 as argument.

Define Functions:

Basic syntax:
```
function(Type1 arg1, Type2 arg2, ...) {
    let va1 = expr1;
    let var2 = expr2;
    return resultExpr;
}
```

You can take as many arguments as you like. But each argument needs a type to be given for it. Use a comma to separate the arguments
when your function does not have any arguments, just do
```
function() {
   ...
}
```

The body of a function is the same as the complete program: First any amount of assigments, then one expression as result.
(Note: As in any other function language, you can define functions a a result, or as Argument)

Function types:
Each function has a specific type: Function<ArgType1, ArgType2, ResultType>
Where the ArgType's are the type of the arguments of the function and ResultType is the result-type of the function.
In above example: triag will have a value of type Function<Number, Number>(Therefore you can see triag to have this type)

Recursion:
Since functions are defined completey anonyminus, there is a special keyword (this), to access the currently defined function:

```
function(Number n) {
    return n = 0
        ? 0
        : this(n - 1);
}
```

This function will cann itself, if given argument is not 0.

Calling functions:
```
fancyFunction(1, 2, 3, 4)
```

This also works without assignment of functions to variables:

```
function(Number n) {
    return n - 5;
}(6)
```

Usage:
Create Parser:
```
final String myCode = ...
final Parser parser = new Parser(myCode);
```

Define predefined functions (Predic itself i naked. There a no functions that are predeined (Except for if). So you need to predefinde them
```
parser.addFunction("leq", new LessOrEqualBody())
```
You can either use some of in the project existing custom functions or define your own. Just Create a class that inherits CustomFunctionBody

You can also add new types to the language (Currently there is not support for generic polymorphism)
```
parser.addTypeGenerator("MyClass", emptyList())
```
Now the parser knows the type "MyClass". See the empty list given. Its for the directiony of its typeparameters.
Currently this is not supported and should be always an emptyList()
You may let your type inherits:
```
parser..addInheritance("MyClass", "SuperObject", l -> l)
```
the last Arguemnt is used to tell, how the type parameters are mapped to the typeparameters of the super-class. Again: No typeparameters, means a simple identical mapping is the only way to go at the moment.
Last but not least run it:

```
Object value = parser.run()
```

The concrete type of the result depends on your program
