package de.blahord.predic.typesystem.generated;

import de.blahord.predic.typesystem.BaseType;
import org.testng.annotations.BeforeMethod;
import org.testng.annotations.Test;

import static com.github.blahord.bettercollections.util.Lists.*;
import static de.blahord.predic.typesystem.Type.singletonType;
import static de.blahord.predic.typesystem.generated.TypeParameterDirection.*;
import static org.fest.assertions.Assertions.assertThat;

@Test(groups = "tests")
public class GeneratedTypeTest {

    private TypeGenerator objectGen;
    private TypeGenerator numberGen;
    private TypeGenerator listGen;
    private TypeGenerator simpleListGen;
    private TypeGenerator woObjectGen;
    private TypeGenerator comparableGen;
    private TypeGenerator longGen;
    private TypeGenerator intGen;

    private BaseType objectType;
    private BaseType numberType;

    private BaseType objectListType;
    private BaseType numberListType;

    private BaseType objetcSimpleListType;
    private BaseType numberSimpleListType;

    private BaseType objectWoObjectType;
    private BaseType numberWoObjectType;

    private GeneratedTypeRegistry generatedTypeRegistry;

    @BeforeMethod
    public void setUp() throws Exception {
        generatedTypeRegistry = new GeneratedTypeRegistry();

        objectGen = new TypeGenerator("Object", emptyList(), generatedTypeRegistry);
        numberGen = new TypeGenerator("Number", emptyList(), generatedTypeRegistry);
        numberGen.addInheritance(objectGen, l -> emptyList());

        listGen = new TypeGenerator("List", asList(OUT), generatedTypeRegistry);
        listGen.addInheritance(objectGen, l -> emptyList());

        simpleListGen = new TypeGenerator("SList", asList(INOUT), generatedTypeRegistry);
        simpleListGen.addInheritance(objectGen, l -> emptyList());

        woObjectGen = new TypeGenerator("Wo", asList(IN), generatedTypeRegistry);
        woObjectGen.addInheritance(objectGen, l -> emptyList());

        comparableGen = new TypeGenerator("Comparable", singletonList(IN), generatedTypeRegistry);
        comparableGen.addInheritance(objectGen, l -> emptyList());

        longGen = new TypeGenerator("Long", emptyList(), generatedTypeRegistry);
        longGen.addInheritance(comparableGen, l -> singletonList(singletonType(longGen.generate(emptyList()))));
        longGen.addInheritance(numberGen, l -> emptyList());

        intGen = new TypeGenerator("Int", emptyList(), generatedTypeRegistry);
        intGen.addInheritance(longGen, l -> emptyList());
        intGen.addInheritance(comparableGen, l -> singletonList(singletonType(intGen.generate(emptyList()))));

        objectType = objectGen.generate(emptyList());
        numberType = numberGen.generate(emptyList());
        objectListType = listGen.generate(singletonList(singletonType(objectType)));
        numberListType = listGen.generate(singletonList(singletonType(numberType)));

        objetcSimpleListType = simpleListGen.generate(singletonList(singletonType(objectType)));
        numberSimpleListType = simpleListGen.generate(singletonList(singletonType(numberType)));

        objectWoObjectType = woObjectGen.generate(singletonList(singletonType(objectType)));
        numberWoObjectType = woObjectGen.generate(singletonList(singletonType(numberType)));
    }

    public void testIsSubType() throws Exception {
        assertThat(numberType.isSubType(objectType)).isTrue();
        assertThat(objectType.isSubType(numberType)).isFalse();
    }

    public void testSubTypeList() throws Exception {
        assertThat(objectListType.isSubType(numberListType)).isFalse();
        assertThat(numberListType.isSubType(objectListType)).isTrue();

        assertThat(objectListType.isSubType(objectListType)).isTrue();
        assertThat(numberListType.isSubType(objectListType)).isTrue();

        assertThat(objectListType.isSubType(numberType)).isFalse();
        assertThat(numberListType.isSubType(numberType)).isFalse();
    }

    public void testSubTypeSimpleList() throws Exception {
        assertThat(objetcSimpleListType.isSubType(numberSimpleListType)).isFalse();
        assertThat(numberSimpleListType.isSubType(objetcSimpleListType)).isFalse();
    }

    public void testSubTypeWoObject() throws Exception {
        assertThat(isSubType(objectWoObjectType, numberWoObjectType)).isTrue();
        assertThat(isSubType(numberWoObjectType, objectWoObjectType)).isFalse();
    }

    public void testRecursiveTypeGen() throws Exception {
        final BaseType longType = longGen.generate(emptyList());
        final BaseType intType = intGen.generate(emptyList());

        assertThat(isSubType(intType, longType)).isTrue();

        final BaseType longComparableType = comparableGen.generate(singletonList(singletonType(longType)));
        final BaseType intComparableType = comparableGen.generate(singletonList(singletonType(intType)));

        assertThat(isSubType(intType, longComparableType)).isTrue();
        assertThat(isSubType(longType, intComparableType)).isTrue();
    }

    public void testToString() throws Exception {
        assertThat(objectType.toString()).isEqualTo("Object");
        assertThat(numberListType.toString()).isEqualTo("List<Number>");

        final TypeGenerator mapGen = new TypeGenerator("Map", asList(OUT, OUT), generatedTypeRegistry);
        final BaseType objectToObjectMapMap = mapGen.generate(asList(singletonType(objectType), singletonType(mapGen.generate( asList(singletonType(objectType), singletonType(numberListType))))));

        assertThat(objectToObjectMapMap.toString()).isEqualTo("Map<Object, Map<Object, List<Number>>>");
    }

    private boolean isSubType(BaseType t1, BaseType t2) {
        return t1.isSubType(t2);
    }
}