package de.blahord.predic.gramma;

import org.apache.commons.math3.fraction.BigFraction;
import org.slf4j.LoggerFactory;
import org.testng.annotations.Test;

import java.io.IOException;
import java.nio.charset.Charset;
import java.nio.file.Files;
import java.nio.file.Paths;

import static org.fest.assertions.Assertions.assertThat;

@Test(groups = "tests")
public class ParserTest {

    public void testParse() throws Exception {
        run("test.predic", new BigFraction(15));
    }

    public void testParse2() throws Exception {
        run("test2.predic", new BigFraction(11));
    }

    public void testParse3() throws Exception {
        run("test3.predic", new BigFraction(15));
    }

    public void testParse4() throws Exception {
        run("test4.predic", new BigFraction(74));
    }

    public void testParse5() throws Exception {
        run("test5.predic", new BigFraction(11));
    }

    public void testParse6() throws Exception {
        run("test6.predic", new BigFraction(55));
    }

    public void testParse7() throws Exception {
        run("test7.predic", "test");
    }

    public void testParse8() throws Exception {
        run("test8.predic", new BigFraction(35));
    }

//    public void testParse9() throws Exception {
//        run("test9.predic", new BigFraction(35));
//    }

    private void run(String fileName, Object expectedValue) throws Exception {
        final Parser parser = new Parser(readFile(fileName, Charset.defaultCharset()));
        assertThat(parser.run()).isEqualTo(expectedValue);
    }

    static String readFile(String path, Charset encoding) throws IOException {
        //Taken from
        // http://stackoverflow.com/questions/4871051/getting-the-current-working-directory-in-java
        final String completePath = System.getProperty("user.dir") + "/src/test/resources/" + path;
        byte[] encoded = Files.readAllBytes(Paths.get(completePath));
        return new String(encoded, encoding);
    }
}