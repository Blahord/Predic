package de.blahord.predic.typesystem;

public abstract class BaseType {

    public abstract boolean isSubType(BaseType superType);

    protected abstract BaseType join(BaseType otherType, BaseType upperBound);

    protected abstract BaseType meet(BaseType otherType, BaseType lowerBound);

    public abstract String toString();

}
