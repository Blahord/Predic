package de.blahord.predic.typesystem.generated;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.map.ExtensibleMap;
import com.github.blahord.bettercollections.map.HashMap;
import de.blahord.predic.typesystem.Type;

import java.util.Objects;

public class GeneratedTypeRegistry {

    private final ExtensibleMap<
        ? extends GenTypeInfo, ? extends GeneratedType,
        ? super GenTypeInfo, ? super GeneratedType
        > types = HashMap.create();

    public GeneratedTypeRegistry() {
    }

    public boolean hasType(TypeGenerator typeGenerator, List<? extends Type> parameters) {
        return types.containsKey(new GenTypeInfo(typeGenerator, parameters));
    }

    public GeneratedType getGeneratedType(TypeGenerator typeGenerator, List<? extends Type> parameters) {
        return types.get(new GenTypeInfo(typeGenerator, parameters));
    }

    public void storeType(TypeGenerator typeGenerator, List<? extends Type> parameters, GeneratedType type) {
        types.put(new GenTypeInfo(typeGenerator, parameters), type);
    }

    private static class GenTypeInfo {

        private final TypeGenerator typeGenerator;
        private final List<? extends Type> parameters;

        private GenTypeInfo(TypeGenerator typeGenerator, List<? extends Type> parameters) {
            this.typeGenerator = typeGenerator;
            this.parameters = ArrayList.create(parameters);
        }

        @Override
        public boolean equals(Object o) {
            if (this == o) return true;
            if (o == null || getClass() != o.getClass()) return false;
            GenTypeInfo that = (GenTypeInfo) o;
            return Objects.equals(typeGenerator, that.typeGenerator) &&
                   Objects.equals(parameters, that.parameters);
        }

        @Override
        public int hashCode() {
            return Objects.hash(typeGenerator, parameters);
        }
    }
}