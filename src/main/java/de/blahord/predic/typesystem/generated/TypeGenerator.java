package de.blahord.predic.typesystem.generated;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import de.blahord.predic.typesystem.Type;

import java.util.Objects;
import java.util.function.Function;

public class TypeGenerator {

    private final Long uuid;
    private final String name;
    private final List<? extends TypeParameterDirection> parameterDirections;

    private ExtensibleList<? extends TypeGenerator, ? super TypeGenerator> inheritances = ArrayList.create();
    private ExtensibleList<
        ? extends Function<? super List<? extends Type>, ? extends List<? extends Type>>,
        ? super Function<? super List<? extends Type>, ? extends List<? extends Type>>> inheritanceTypeMapping = ArrayList.create();

    private final GeneratedTypeRegistry generatedTypeRegistry;

    public TypeGenerator(String name, List<? extends TypeParameterDirection> parameterDirections, GeneratedTypeRegistry generatedTypeRegistry) {
        this.name = name;
        this.parameterDirections = ArrayList.create(parameterDirections);
        this.generatedTypeRegistry = generatedTypeRegistry;

        uuid = generateUUID();
    }

    public void addInheritance(TypeGenerator typeGenerator, Function<? super List<? extends Type>, ? extends List<? extends Type>> mapping) {
        inheritances.add(typeGenerator);
        inheritanceTypeMapping.add(mapping);
    }

    public List<? extends TypeParameterDirection> getParameterDirections() {
        return parameterDirections;
    }

    public GeneratedType generate(List<? extends Type> typeParameter) {
        if (generatedTypeRegistry.hasType(this, typeParameter)) {
            return generatedTypeRegistry.getGeneratedType(this, typeParameter);
        }

        final GeneratedType generatedType = new GeneratedType(this, typeParameter);
        generatedTypeRegistry.storeType(this, typeParameter, generatedType);

        for (int i = 0; i < inheritances.size(); i++) {
            generatedType.addInheritedType(
                inheritances.at(i).generate(inheritanceTypeMapping.at(i).apply(typeParameter))
            );
        }

        return generatedType;
    }

    public String getName() {
        return name;
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final TypeGenerator that = (TypeGenerator) o;
        return Objects.equals(uuid, that.uuid);
    }

    @Override
    public int hashCode() {
        return Objects.hash(uuid);
    }

    private static long generateUUID() {
        long uuid = 0;
        for (int i = 0; i < 63; i++) {
            uuid = 2L * uuid + (Math.random() >= 0.5 ? 1L : 0L);
        }

        return uuid;
    }
}
