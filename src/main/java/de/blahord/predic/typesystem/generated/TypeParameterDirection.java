package de.blahord.predic.typesystem.generated;

public enum TypeParameterDirection {
    IN,
    OUT,
    INOUT;
}
