package de.blahord.predic.typesystem.generated;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.map.Map;
import com.github.blahord.bettercollections.set.ExtensibleSet;
import com.github.blahord.bettercollections.set.HashSet;
import com.github.blahord.bettercollections.util.Maps;
import de.blahord.predic.typesystem.BaseType;
import de.blahord.predic.typesystem.Type;
import org.apache.commons.math3.util.Pair;

import java.util.function.BinaryOperator;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static com.github.blahord.bettercollections.util.Maps.asMap;
import static de.blahord.predic.typesystem.generated.TypeParameterDirection.IN;
import static de.blahord.predic.typesystem.generated.TypeParameterDirection.OUT;

public class GeneratedType extends BaseType {

    private enum CombineType {
        JOIN,
        MEET
    }

    private static final Map<
        ? extends Pair<? extends TypeParameterDirection, ? extends CombineType>,
        ? extends BinaryOperator<Type>
        > COMBINE_OPERATIONS_MAP = asMap(
        Maps.<Pair<? extends TypeParameterDirection, ? extends CombineType>, BinaryOperator<Type>>entry(
            Pair.create(IN, CombineType.JOIN),
            Type::meet
        ),
        Maps.<Pair<? extends TypeParameterDirection, ? extends CombineType>, BinaryOperator<Type>>entry(
            Pair.create(IN, CombineType.MEET),
            Type::join
        ),
        Maps.<Pair<? extends TypeParameterDirection, ? extends CombineType>, BinaryOperator<Type>>entry(
            Pair.create(OUT, CombineType.JOIN),
            Type::join
        ),
        Maps.<Pair<? extends TypeParameterDirection, ? extends CombineType>, BinaryOperator<Type>>entry(
            Pair.create(OUT, CombineType.MEET),
            Type::meet
        )
    );

    private final TypeGenerator typeGenerator;
    private final List<? extends Type> typeParameters;

    private ExtensibleSet<? extends GeneratedType, ? super GeneratedType> inheritedTypes = HashSet.create();

    protected GeneratedType(TypeGenerator typeGenerator, List<? extends Type> typeParameters) {
        this.typeGenerator = typeGenerator;
        this.typeParameters = typeParameters;
    }

    @Override
    public boolean isSubType(BaseType superType) {
        if (!(superType instanceof GeneratedType)) {
            return false;
        }
        final GeneratedType otherGeneratedType = (GeneratedType) superType;

        if (typeGenerator.equals(otherGeneratedType.typeGenerator)) {
            return checkParameters(this, otherGeneratedType);
        } else {
            return inheritedTypes.stream().anyMatch(t -> t.isSubType(superType));
        }
    }

    @Override
    protected BaseType join(BaseType otherType, BaseType upperBound) {
        if (!(otherType instanceof GeneratedType)) {
            return upperBound;
        }

        final GeneratedType generatedType = (GeneratedType) otherType;

        if (!typeGenerator.equals(generatedType.typeGenerator)) {
            return upperBound;
        }

        return typeGenerator.generate(combineTypeParameters(generatedType, CombineType.JOIN));
    }

    @Override
    protected BaseType meet(BaseType otherType, BaseType lowerBound) {
        if (!(otherType instanceof GeneratedType)) {
            return lowerBound;
        }

        final GeneratedType generatedType = (GeneratedType) otherType;

        if (!typeGenerator.equals(generatedType.typeGenerator)) {
            return lowerBound;
        }

        return typeGenerator.generate(combineTypeParameters(generatedType, CombineType.MEET));
    }

    public void addInheritedType(GeneratedType generatedType) {
        inheritedTypes.add(generatedType);
    }

    public List<? extends Type> getTypeParameters() {
        return typeParameters;
    }

    @Override
    public String toString() {
        final StringBuilder stringBuilder = new StringBuilder();

        boolean first = true;
        for (Type t : in(typeParameters)) {
            if (!first) {
                stringBuilder.append(", ");
            }

            stringBuilder.append(t.toString());

            first = false;
        }

        final String typeParameterList = stringBuilder.length() > 0 ? "<" + stringBuilder + ">" : "";
        return typeGenerator.getName() + typeParameterList;
    }

    private static boolean checkParameters(GeneratedType t1, GeneratedType t2) {
        for (int i = 0; i < t1.typeParameters.size(); i++) {
            final Type thisParam = t1.typeParameters.at(i);
            final Type otherParam = t2.typeParameters.at(i);
            final TypeParameterDirection direction = t1.typeGenerator.getParameterDirections().at(i);

            if (!checkParam(direction, thisParam, otherParam)) {
                return false;
            }

        }
        return true;
    }

    private static boolean checkParam(TypeParameterDirection direction, Type t1, Type t2) {
        switch (direction) {
            case IN:
                return Type.isSubType(t2, t1);
            case OUT:
                return Type.isSubType(t1, t2);
            case INOUT:
                return Type.isTypeEqual(t1, t2);
            default:
                throw new IllegalArgumentException("Unhandled direction: " + direction);
        }
    }

    private List<? extends Type> combineTypeParameters(GeneratedType generatedType, CombineType combineType) {
        final ExtensibleList<? extends Type, ? super Type> typeParamList = ArrayList.create();
        for (int i = 0; i < typeParameters.size(); i++) {
            typeParamList.add(
                COMBINE_OPERATIONS_MAP.get(
                    Pair.create(typeGenerator.getParameterDirections().at(i), combineType)
                ).apply(
                    typeParameters.at(i),
                    generatedType.typeParameters.at(i)
                ));
        }
        return typeParamList;
    }
}