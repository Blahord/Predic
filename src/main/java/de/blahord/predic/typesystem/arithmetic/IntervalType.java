package de.blahord.predic.typesystem.arithmetic;

import de.blahord.predic.typesystem.BaseType;

public class IntervalType extends BaseType {

    private final long minValue;
    private final long maxValue;

    public IntervalType(Long minValue, Long maxValue) {
        this.minValue = minValue == null ? Long.MIN_VALUE : minValue;
        this.maxValue = maxValue == null ? Long.MAX_VALUE : maxValue;
    }

    @Override
    public boolean isSubType(BaseType superType) {
        if (!(superType instanceof IntervalType)) {
            return false;
        }

        final IntervalType otherIntervalType = (IntervalType) superType;

        return minValue > maxValue ||
               checkBounds(this, otherIntervalType);
    }

    @Override
    protected BaseType join(BaseType otherType, BaseType upperBound) {
        if (!(otherType instanceof IntervalType)) {
            return upperBound;
        }

        final IntervalType intervalType = (IntervalType) otherType;

        return new IntervalType(getMin(minValue, intervalType.minValue), getMax(maxValue, intervalType.maxValue));
    }

    @Override
    protected BaseType meet(BaseType otherType, BaseType lowerBound) {
        if (!(otherType instanceof IntervalType)) {
            return lowerBound;
        }

        final IntervalType intervalType = (IntervalType) otherType;

        return new IntervalType(getMax(minValue, intervalType.minValue), getMin(maxValue, intervalType.maxValue));
    }

    @Override
    public String toString() {
        return "{" +
               (minValue + " <= ") +
               "v" +
               (" <= " + maxValue) +
               "}";
    }

    private static boolean checkBounds(IntervalType t1, IntervalType t2) {
        return checkLowerBound(t1.minValue, t2.maxValue) &&
               checkUpperBound(t1.maxValue, t2.maxValue);

    }

    private static boolean checkLowerBound(long v1, long v2) {
        return (v2 <= v1);
    }

    private static boolean checkUpperBound(long v1, long v2) {
        return (v2 >= v1);
    }

    private static Long getMin(Long l1, Long l2) {
        return Math.min(l1, l2);
    }

    private static Long getMax(Long l1, Long l2) {
        return Math.max(l1, l2);
    }

}
