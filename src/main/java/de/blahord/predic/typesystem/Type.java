package de.blahord.predic.typesystem;

import com.github.blahord.bettercollections.collection.Collection;
import com.github.blahord.bettercollections.set.ExtensibleSet;
import com.github.blahord.bettercollections.set.HashSet;
import com.github.blahord.bettercollections.set.Set;
import com.github.blahord.bettercollections.stream.Collectors;
import com.github.blahord.bettercollections.util.Sets;

import static com.github.blahord.bettercollections.compatibility.SetWrappers.asJavaUtilSet;
import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static com.github.blahord.bettercollections.util.Sets.emptySet;
import static com.github.blahord.bettercollections.util.Sets.singletonSet;

public class Type {

    private final Set<? extends Set<? extends BaseType>> baseTypeDNF;

    public Type(Collection<? extends Collection<? extends BaseType>> baseTypeDNF) {
        final ExtensibleSet<? extends Set<? extends BaseType>, ? super Set<? extends BaseType>> tmpBaseDNF = HashSet.create();
        baseTypeDNF.forEach(
            conj -> tmpBaseDNF.add(HashSet.create(conj))
        );

        this.baseTypeDNF = tmpBaseDNF;
    }

    public boolean isSubType(Type superType) {
        return baseTypeDNF.stream().allMatch(
            thisConj -> superType.baseTypeDNF.stream().anyMatch(
                thatConj -> thisConj.stream().anyMatch(
                    thisBt -> thatConj.stream().allMatch(
                        thatBt -> thisBt.isSubType(thatBt)
                    )
                )
            )
        );
    }

    public <T extends BaseType> T projectToBaseType(T lowerBound, T upperBound, Class<? extends T> resultClass) {
        if (baseTypeDNF.isEmpty()) {
            return lowerBound;
        }

        final Set<? extends T> meets = getMeets(lowerBound, upperBound, resultClass);

        T result = lowerBound;
        for (T meet : in(meets)) {
            result = (T) result.join(meet, upperBound);
        }

        return result;
    }

    public static Type singletonType(BaseType baseType) {
        return new Type(singletonSet(singletonSet(baseType)));
    }

    public static Type anythingType() {
        return new Type(singletonSet(Sets.<BaseType>emptySet()));
    }

    public static Type nothingType() {
        return new Type(emptySet());
    }

    public static boolean isSubType(Type subType, Type superType) {
        return subType.isSubType(superType);
    }

    public static boolean isTypeEqual(Type t1, Type t2) {
        return isSubType(t1, t2) && isSubType(t2, t1);
    }

    public static Type join(Type t1, Type t2) {
        if (isSubType(t1, t2)) {
            return t2;
        }
        if (isSubType(t2, t1)) {
            return t1;
        }

        final ExtensibleSet<? extends Collection<? extends BaseType>, ? super Collection<? extends BaseType>> combinedTypes = HashSet.create();
        combinedTypes.addAll(t1.baseTypeDNF);
        combinedTypes.addAll(t2.baseTypeDNF);

        return new Type(combinedTypes);
    }

    public static Type meet(Type t1, Type t2) {
        if (isSubType(t1, t2)) {
            return t1;
        }
        if (isSubType(t2, t1)) {
            return t2;
        }

        final ExtensibleSet<? extends Set<? extends BaseType>, ? super Set<? extends BaseType>> res = HashSet.create();
        for (Set<? extends BaseType> conj1 : in(t1.baseTypeDNF)) {
            for (Set<? extends BaseType> conj2 : in(t2.baseTypeDNF)) {
                final ExtensibleSet<? extends BaseType, ? super BaseType> mergeSet = HashSet.create(conj1);
                mergeSet.addAll(conj2);
                res.add(mergeSet);
            }
        }

        return new Type(res);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Type type = (Type) o;
        return isTypeEqual(this, type);
    }

    @Override
    public int hashCode() {
        return 0; //Because a different structure may be define an equal type
    }

    @Override
    public String toString() {
        if (baseTypeDNF.isEmpty()) {
            return "nothing";
        }
        if (baseTypeDNF.size() == 1) {
            return joinConj(baseTypeDNF.iterator().next(), false);
        }
        return String.join("|", asJavaUtilSet(baseTypeDNF.stream().map(conj -> "(" + joinConj(conj, true) + ")").collect(Collectors.toHashSet())));
    }

    private String joinConj(Set<? extends BaseType> conj, boolean withBraces) {
        if (conj.isEmpty()) return "anything";
        if (conj.size() == 1) return conj.iterator().next().toString();

        final String result = String.join("&", asJavaUtilSet(conj.stream().map(BaseType::toString).collect(Collectors.toHashSet())));

        if (withBraces) {
            return "(" + result + ")";
        }
        return result;
    }

    private <T extends BaseType> Set<? extends T> getMeets(T lowerBound, T upperBound, Class<? extends T> baseTypeClass) {
        final ExtensibleSet<? extends T, ? super T> meets = HashSet.create();
        for (Set<? extends BaseType> conj : in(baseTypeDNF)) {
            meets.add(meetConj(conj, lowerBound, upperBound, baseTypeClass));
        }

        return meets;
    }

    private <T extends BaseType> T meetConj(Set<? extends BaseType> conj, T lowerBound, T upperBound, Class<? extends T> baseTypeClass) {
        final Set<? extends T> matching = conj.stream()
                                              .filter(bt -> baseTypeClass == bt.getClass())
                                              .filter(bt -> bt.isSubType(upperBound))
                                              .map(bt -> lowerBound.isSubType(bt) ? bt : lowerBound)
                                              .map(bt -> (T) bt)
                                              .collect(Collectors.toHashSet());

        T result = upperBound;
        for (T baseType : in(matching)) {
            result = (T) result.meet(baseType, lowerBound);
        }

        return result;
    }
}
