package de.blahord.predic.typesystem.special;

import de.blahord.predic.typesystem.BaseType;

public class NothingType extends BaseType {

    public static final NothingType INSTANCE = new NothingType();

    private NothingType() {
    }

    @Override
    public boolean isSubType(BaseType superType) {
        return true;
    }

    @Override
    protected BaseType join(BaseType otherType, BaseType upperBound) {
        return otherType;
    }

    @Override
    protected BaseType meet(BaseType otherType, BaseType lowerBound) {
        return INSTANCE;
    }

    @Override
    public String toString() {
        return "nothing";
    }
}
