package de.blahord.predic.typesystem.special;

import de.blahord.predic.typesystem.BaseType;

public class AnythingType extends BaseType {

    public final static AnythingType INSTANCE = new AnythingType();

    private AnythingType() {
    }

    @Override
    public boolean isSubType(BaseType superType) {
        return superType instanceof AnythingType;
    }

    @Override
    protected BaseType join(BaseType otherType, BaseType upperBound) {
        return INSTANCE;
    }

    @Override
    protected BaseType meet(BaseType otherType, BaseType lowerBound) {
        return otherType;
    }

    @Override
    public String toString() {
        return "anything";
    }
}
