package de.blahord.predic.ast;

public class IfExpression extends Expression {

    private final Expression condExpression;
    private final Expression thenExpression;
    private final Expression elseExpression;

    public IfExpression(Expression condExpression, Expression thenExpression, Expression elseExpression) {
        super(condExpression.getCodeRange().join(elseExpression.getCodeRange()));

        this.condExpression = condExpression;
        this.thenExpression = thenExpression;
        this.elseExpression = elseExpression;
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        final Boolean condValue = (Boolean) condExpression.evaluate(evalContext);

        if (condValue) {
            return thenExpression.evaluate(evalContext);
        }
        else {
            return  elseExpression.evaluate(evalContext);
        }
    }
}
