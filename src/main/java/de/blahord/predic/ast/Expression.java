package de.blahord.predic.ast;

import de.blahord.predic.typesystem.Type;

public abstract class Expression extends ASTNode {

    private Type type = Type.nothingType();

    public Expression(CodeRange codeRange) {
        super(codeRange);
    }

    public Type getType() {
        return type;
    }

    public void setType(Type type) {
        this.type = type;
    }

    public abstract Object evaluate(EvalContext evalContext);
}
