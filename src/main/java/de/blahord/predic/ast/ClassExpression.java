package de.blahord.predic.ast;

public class ClassExpression extends Expression {

    private final String name;

    public ClassExpression(CodeRange codeRange, String name) {
        super(codeRange);
        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        return null;
    }
}
