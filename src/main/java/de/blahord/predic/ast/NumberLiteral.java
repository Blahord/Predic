package de.blahord.predic.ast;

import org.apache.commons.math3.fraction.BigFraction;

public class NumberLiteral extends Literal {

    private final BigFraction value;

    public NumberLiteral(CodeRange codeRange, BigFraction value) {
        super(codeRange);

        this.value = value;
    }

    @Override
    public String toString() {
        return value.toString();
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        return value;
    }
}
