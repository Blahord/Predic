package de.blahord.predic.ast;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.MutableList;
import com.github.blahord.bettercollections.map.ExtensibleMap;
import com.github.blahord.bettercollections.map.HashMap;
import com.github.blahord.bettercollections.map.Map;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;

public class EvalContext {

    private final MutableList<? extends EvalContextFrame, ? super EvalContextFrame> contextFrames = ArrayList.create();

    public EvalContext() {
        pushContext();
    }

    public void pushContext() {
        contextFrames.add(new EvalContextFrame());
    }

    public void popContext() {
        contextFrames.removeAt(contextFrames.size() - 1);
    }

    private EvalContextFrame getCurrentContext() {
        return contextFrames.at(contextFrames.size()-1);
    }

    public void putValue(Symbol symbol, Object value) {
        getCurrentContext().putValue(symbol, value);
    }

    public Map<? extends Symbol, ?> getFlatContext() {
        ExtensibleMap<? extends Symbol, ?, ? super Symbol, ? super Object> result = HashMap.create();

        for (EvalContextFrame evalContextFrame : in(contextFrames)) {
            evalContextFrame.values.forEach(result::put);
        }

        return result;
    }

    public Object getValue(Symbol symbol) {
        for (int i = contextFrames.size()-1; i >= 0; i--) {
            if (contextFrames.at(i).containsSymbol(symbol)) {
                return contextFrames.at(i).getValue(symbol);
            }
        }

        return null;
    }

    private final class EvalContextFrame {

        private final ExtensibleMap<
            ? extends Symbol, ?,
            ? super Symbol, ? super Object
            > values = HashMap.create();

        void putValue(Symbol symbol, Object value) {
            values.put(symbol, value);
        }

        boolean containsSymbol(Symbol symbol) {
            return values.containsKey(symbol);
        }

        Object getValue(Symbol symbol) {
            return values.get(symbol);
        }
    }
}
