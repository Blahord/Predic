package de.blahord.predic.ast;

public class Assignment extends ASTNode {

    private final Symbol symbol;
    private final Expression expression;

    public Assignment(Symbol symbol, Expression expression) {
        super(symbol.getCodeRange().join(expression.getCodeRange()));

        this.symbol = symbol;
        this.expression = expression;
    }

    public void evaluate(EvalContext evalContext) {
        evalContext.putValue(symbol, expression.evaluate(evalContext));
    }
}
