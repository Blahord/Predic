package de.blahord.predic.ast;

public class Member extends ASTNode {

    private final Symbol name;
    private final Expression expression;

    public Member(Symbol name, Expression expression) {
        super(name.getCodeRange().join(expression.getCodeRange()));

        this.name = name;
        this.expression = expression;
    }

    public Symbol getName() {
        return name;
    }

    public Expression getExpression() {
        return expression;
    }
}
