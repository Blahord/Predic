package de.blahord.predic.ast;

import de.blahord.predic.typesystem.Type;

public class Parameter extends ASTNode {

    private final Type type;
    private final Symbol symbol;

    public Parameter(CodeRange codeRange, Type type, Symbol symbol) {
        super(codeRange);

        this.type = type;
        this.symbol = symbol;
    }

    public Type getType() {
        return type;
    }

    public Symbol getSymbol() {
        return symbol;
    }
}
