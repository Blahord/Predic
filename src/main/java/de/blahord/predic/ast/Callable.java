package de.blahord.predic.ast;

import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.map.HashMap;
import com.github.blahord.bettercollections.map.Map;
import com.github.blahord.bettercollections.map.MutableMap;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;

public abstract class Callable {

    private final List<? extends Parameter> parameter;
    private final MutableMap<? extends Symbol, ?,
        ? super Symbol, ? super Object> assignments = HashMap.create();

    protected Callable(List<? extends Parameter> parameter, EvalContext evalContext) {
        this.parameter = parameter;

        assignments.putAll(evalContext.getFlatContext());
        assignments.remove(new Symbol(CodeRange.EMPTY, "this"));
    }

    abstract Object run(EvalContext evalContext);

    public List<? extends Parameter> getParameter() {
        return parameter;
    }

    public void runFunctionContextAssignments(EvalContext evalContext) {
        for (Map.Entry<? extends Symbol, ?> assignment : in(assignments.entrySet())) {
            evalContext.putValue(assignment.getKey(), assignment.getValue());
        }
    }

}
