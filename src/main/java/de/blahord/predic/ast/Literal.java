package de.blahord.predic.ast;

public abstract class Literal extends Expression {

    public Literal(CodeRange codeRange) {
        super(codeRange);
    }
}
