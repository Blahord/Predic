package de.blahord.predic.ast;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.map.ExtensibleMap;
import com.github.blahord.bettercollections.map.HashMap;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.typesystem.Type;
import de.blahord.predic.typesystem.generated.GeneratedType;

public class FunctionCall extends Expression {

    private final Expression expression;
    private final List<? extends Expression> arguments;

    public FunctionCall(CodeRange codeRange, Expression expression, List<? extends Expression> arguments, ParserContext parserContext) {
        super(codeRange);

        this.expression = expression;
        this.arguments = arguments;

        final GeneratedType highestFunctionType = getHighestFunctionType(parserContext, arguments.size() + 1);
        final GeneratedType lowestFunctionType = getLowestFunctionType(parserContext, arguments.size() + 1);

        final GeneratedType functionType = expression.getType().projectToBaseType(
            lowestFunctionType,
            highestFunctionType,
            GeneratedType.class
        );

        setType(functionType.getTypeParameters().at(functionType.getTypeParameters().size() - 1));
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        final Callable function = (Callable) expression.evaluate(evalContext);

        final ExtensibleMap<? extends Symbol, ?, ? super Symbol, ? super Object> argsMapping = HashMap.create();
        for (int i = 0; i < arguments.size(); i++) {
            argsMapping.put(function.getParameter().at(i).getSymbol(), arguments.at(i).evaluate(evalContext));
        }

        evalContext.pushContext();

        function.runFunctionContextAssignments(evalContext);
        argsMapping.forEach(evalContext::putValue);
        evalContext.putValue(new Symbol(CodeRange.EMPTY, "this"), function);

        evalContext.pushContext();
        final Object value = function.run(evalContext);
        evalContext.popContext();

        evalContext.popContext();

        return value;
    }

    private static GeneratedType getLowestFunctionType(ParserContext parserContext, int numberParams) {
        return parserContext.generateGeneratedType(Symbol.SYMBOL_FUNCTION, getFunctionTypeArgs(Type.anythingType(), Type.nothingType(), numberParams));
    }

    private static GeneratedType getHighestFunctionType(ParserContext parserContext, int numberParams) {
        return parserContext.generateGeneratedType(Symbol.SYMBOL_FUNCTION, getFunctionTypeArgs(Type.nothingType(), Type.anythingType(), numberParams));
    }

    private static List<? extends Type> getFunctionTypeArgs(Type in, Type out, int count) {
        final ExtensibleList<? extends Type, ? super Type> typeArgs = ArrayList.create();
        for (int i = 0; i < count - 1; i++) {
            typeArgs.add(in);
        }
        typeArgs.add(out);

        return typeArgs;
    }
}
