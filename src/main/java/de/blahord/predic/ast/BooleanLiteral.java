package de.blahord.predic.ast;

public class BooleanLiteral extends Literal {

    private final boolean value;

    public BooleanLiteral(CodeRange codeRange, boolean value) {
        super(codeRange);
        this.value = value;
    }

    @Override
    public String toString() {
        return value + "";
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        return value;
    }
}
