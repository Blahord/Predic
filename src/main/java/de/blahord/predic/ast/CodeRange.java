package de.blahord.predic.ast;

import static java.lang.Long.MAX_VALUE;
import static java.lang.Long.MIN_VALUE;

public class CodeRange {

    public static final CodeRange EMPTY = new CodeRange(MAX_VALUE, MAX_VALUE, MIN_VALUE, MIN_VALUE);

    private final long beginLine;
    private final long beginColumn;

    private final long endLine;
    private final long endColumn;

    public CodeRange(long beginLine, long beginColumn, long endLine, long endColumn) {
        this.beginLine = beginLine;
        this.beginColumn = beginColumn;
        this.endLine = endLine;
        this.endColumn = endColumn;
    }

    public long getBeginLine() {
        return beginLine;
    }

    public long getBeginColumn() {
        return beginColumn;
    }

    public long getEndLine() {
        return endLine;
    }

    public long getEndColumn() {
        return endColumn;
    }

    public CodeRange join(CodeRange other) {
        final boolean thisHasLowerBegin = isLowerBegin(other);
        final boolean thisHasLowerEnd = isLowerEnd(other);

        return new CodeRange(
            thisHasLowerBegin ? beginLine : other.beginLine,
            thisHasLowerBegin ? beginColumn : other.beginColumn,
            thisHasLowerEnd ? endLine : other.endLine,
            thisHasLowerEnd ? endColumn : other.endColumn
        );
    }

    private boolean isLowerBegin(CodeRange other) {
        return beginLine < other.beginLine ||
               (beginLine == other.beginLine && beginColumn < other.beginColumn);
    }

    private boolean isLowerEnd(CodeRange other) {
        return endLine < other.endLine ||
               (endLine == other.endLine && endColumn < other.endColumn);
    }
}
