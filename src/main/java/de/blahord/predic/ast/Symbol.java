package de.blahord.predic.ast;

import java.util.Objects;

public class Symbol extends Expression {

    public static final Symbol SYMBOL_FUNCTION = new Symbol(CodeRange.EMPTY, "Function");
    public static final Symbol SYMBOL_OBJECT = new Symbol(CodeRange.EMPTY, "Object");
    public static final Symbol SYMBOL_STRING = new Symbol(CodeRange.EMPTY, "String");
    public static final Symbol SYMBOL_NUMBER = new Symbol(CodeRange.EMPTY, "Number");
    public static final Symbol SYMBOL_BOOLEAN = new Symbol(CodeRange.EMPTY, "Boolean");

    private final String name;

    public Symbol(CodeRange codeRange, String name) {
        super(codeRange);

        this.name = name;
    }

    public String getName() {
        return name;
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        return evalContext.getValue(this);
    }

    @Override
    public boolean equals(Object o) {
        if (this == o) return true;
        if (o == null || getClass() != o.getClass()) return false;
        final Symbol symbol = (Symbol) o;
        return Objects.equals(name, symbol.name);
    }

    @Override
    public int hashCode() {
        return Objects.hash(name);
    }

    @Override
    public String toString() {
        return name;
    }
}
