package de.blahord.predic.ast;

import com.github.blahord.bettercollections.map.ExtensibleMap;
import com.github.blahord.bettercollections.map.HashMap;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;

public class Constructor extends Callable {

    private ClassDefinition classDefinition;

    public Constructor(ClassDefinition classDefinition, EvalContext evalContext) {
        super(classDefinition.getParameter(), evalContext);
        this.classDefinition = classDefinition;
    }

    @Override
    Object run(EvalContext evalContext) {
        final ExtensibleMap<? extends String, ?, ? super String, ? super Object> object = HashMap.create();

        evalContext.putValue(new Symbol(CodeRange.EMPTY, "this"), object);

        for (Member member : in(classDefinition.getMembers())) {
            final Object value = member.getExpression().evaluate(evalContext);
            final Symbol memberName = member.getName();

            evalContext.putValue(memberName, value);
            object.put(memberName.getName(), value);
        }

        return object;
    }
}
