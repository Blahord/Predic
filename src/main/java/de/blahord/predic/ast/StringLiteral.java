package de.blahord.predic.ast;

public class StringLiteral extends Literal {

    private final String text;

    public StringLiteral(CodeRange codeRange, String text) {
        super(codeRange);

        this.text = text.substring(1, text.length()-1);
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        return text;
    }

    @Override
    public String toString() {
        return "\""+text+"\"";
    }
}
