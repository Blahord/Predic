package de.blahord.predic.ast;

import com.github.blahord.bettercollections.map.Map;

public class FieldAccessExpression extends Expression{

    private final Expression left;
    private final Symbol field;

    public FieldAccessExpression(Expression left, Symbol field) {
        super(left.getCodeRange().join(field.getCodeRange()));

        this.left = left;
        this.field = field;
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        //noinspection unchecked
        final Map<? extends String, ?> object = (Map<? extends String, ?>) left.evaluate(evalContext);

        return object.get(field.getName());
    }
}
