package de.blahord.predic.ast;

import org.apache.commons.math3.fraction.BigFraction;

public class ArithmeticExpression extends Expression {

    private final Expression expr1;
    private final String operator;
    private final Expression expr2;


    public ArithmeticExpression(Expression expr1, String operator, Expression expr2) {
        super(expr1.getCodeRange().join(expr2.getCodeRange()));

        this.expr1 = expr1;
        this.operator = operator;
        this.expr2 = expr2;
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        final BigFraction val1 = (BigFraction) expr1.evaluate(evalContext);
        final BigFraction val2 = (BigFraction) expr2.evaluate(evalContext);

        switch (operator) {
            case "+":
                return val1.add(val2);
            case "-":
                return val1.subtract(val2);
            case "*":
                return val1.multiply(val2);
            case "/":
                return val1.divide(val2);
            case "<":
                return val1.compareTo(val2) < 0;
            case "<=":
                return val1.compareTo(val2) <= 0;
            case ">":
                return val1.compareTo(val2) > 0;
            case ">=":
                return val1.compareTo(val2) >= 0;
            case "=":
                return val1.compareTo(val2) == 0;
            default:
                return null;
        }
    }
}
