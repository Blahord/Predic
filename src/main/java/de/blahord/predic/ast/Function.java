package de.blahord.predic.ast;

public class Function extends Callable {

    private final FunctionDefinition functionDefinition;

    public Function(FunctionDefinition functionDefinition, EvalContext evalContext) {
        super(functionDefinition.getParameters(), evalContext);

        this.functionDefinition = functionDefinition;
    }

    @Override
    public Object run(EvalContext evalContext) {
        functionDefinition.getBody().runAssignments(evalContext);

        return functionDefinition.getBody().evaluate(evalContext);
    }

    public FunctionDefinition getFunctionDefinition() {
        return functionDefinition;
    }
}
