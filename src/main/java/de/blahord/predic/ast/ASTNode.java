package de.blahord.predic.ast;

public class ASTNode {

    private final CodeRange codeRange;

    public ASTNode(CodeRange codeRange) {
        this.codeRange = codeRange;
    }

    public CodeRange getCodeRange() {
        return codeRange;
    }
}
