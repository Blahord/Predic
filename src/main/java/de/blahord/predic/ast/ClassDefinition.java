package de.blahord.predic.ast;

import com.github.blahord.bettercollections.list.List;

public class ClassDefinition extends ASTNode {

    private final Symbol name;
    private List<? extends Parameter> parameter;
    private final List<? extends Member> members;

    public ClassDefinition(CodeRange codeRange, Symbol name, List<? extends Parameter> parameter, List<? extends Member> members) {
        super(codeRange);

        this.name = name;
        this.parameter = parameter;
        this.members = members;
    }

    public Symbol getName() {
        return name;
    }

    public void addMember(Member member) {

    }

    public Object eval(EvalContext evalContext) {
        return new Constructor(this, evalContext);
    }

    public List<? extends Parameter> getParameter() {
        return parameter;
    }

    public List<? extends Member> getMembers() {
        return members;
    }

}
