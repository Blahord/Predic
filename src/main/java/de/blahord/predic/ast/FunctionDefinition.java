package de.blahord.predic.ast;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.stream.Collectors.toArrayList;

public class FunctionDefinition extends Expression {

    private final List<? extends Parameter> parameters;
    private Body body;

    public FunctionDefinition(CodeRange codeRange, List<? extends Parameter> parameters) {
        super(codeRange);
        this.parameters = parameters;
    }

    public Body getBody() {
        return body;
    }

    public void setBody(Body body) {
        this.body = body;
    }

    public List<? extends Parameter> getParameters() {
        return parameters;
    }

    @Override
    public Object evaluate(EvalContext evalContext) {
        return new Function(this, evalContext);
    }

    public void setReturnType(Type returnType, ParserContext parserContext) {
        final ExtensibleList<? extends Type, ? super Type> functionTypesList = parameters.stream().map(Parameter::getType).collect(toArrayList());
        functionTypesList.add(returnType);

        setType(parserContext.generateType(Symbol.SYMBOL_FUNCTION, functionTypesList));
    }
}
