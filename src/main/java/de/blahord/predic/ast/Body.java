package de.blahord.predic.ast;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.List;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;

public class Body extends ASTNode {

    private final List<? extends ClassDefinition> classes;
    private final List<? extends Assignment> assignments;
    private final Expression expression;

    public Body(CodeRange codeRange, List<? extends ClassDefinition> classes, List<? extends Assignment> assignments, Expression expression) {
        super(codeRange);

        this.classes = classes;
        this.assignments = ArrayList.create(assignments);
        this.expression = expression;
    }

    public Expression getExpression() {
        return expression;
    }

    public Object evaluate(EvalContext evalContext) {
        evalContext.pushContext();

        for (ClassDefinition classDefinition : in(classes)) {
            evalContext.putValue(classDefinition.getName(), classDefinition.eval(evalContext));
        }

        runAssignments(evalContext);
        final Object value = expression.evaluate(evalContext);
        evalContext.popContext();

        return value;
    }

    protected void runAssignments(EvalContext evalContext) {
        assignments.forEach(a -> a.evaluate(evalContext));
    }
}
