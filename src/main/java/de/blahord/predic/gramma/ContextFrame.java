package de.blahord.predic.gramma;

import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.map.ExtensibleMap;
import com.github.blahord.bettercollections.map.HashMap;
import de.blahord.predic.ast.Expression;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.typesystem.generated.GeneratedTypeRegistry;
import de.blahord.predic.typesystem.generated.TypeGenerator;
import de.blahord.predic.typesystem.generated.TypeParameterDirection;

public class ContextFrame {

    private final ExtensibleMap<
        ? extends Symbol, ? extends TypeGenerator,
        ? super Symbol, ? super TypeGenerator
        > knownTypeGenerators = HashMap.create();

    private final ExtensibleMap<
        ? extends Symbol, ? extends Expression,
        ? super Symbol, ? super Expression
        > knownSymbols = HashMap.create();

    public ContextFrame() {
    }

    public TypeGenerator getTypeGenerator(Symbol classSymbol) {
        return knownTypeGenerators.get(classSymbol);
    }

    public void addTypeGenerator(Symbol classSymbol, List<? extends TypeParameterDirection> typeParameterDirections, GeneratedTypeRegistry generatedTypeRegistry) {
        knownTypeGenerators.put(classSymbol, new TypeGenerator(classSymbol.getName(), typeParameterDirections, generatedTypeRegistry));
    }

    public Expression getExpression(Symbol symbol) {
        return knownSymbols.get(symbol);
    }

    public void addExpression(Symbol symbol, Expression expression) {
        if (expression != null) {
            symbol.setType(expression.getType());
        }

        knownSymbols.put(symbol, expression);
    }

    public boolean isSymbolKnown(Symbol symbol) {
        return knownSymbols.containsKey(symbol);
    }

    public Symbol getSymbol(String symbolName) {
        return knownSymbols.keySet().stream().filter(symbol -> symbol.getName().equals(symbolName)).findFirst().orElse(null);
    }
}
