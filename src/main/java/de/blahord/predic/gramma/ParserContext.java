package de.blahord.predic.gramma;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.list.MutableList;
import com.github.blahord.bettercollections.map.ExtensibleMap;
import com.github.blahord.bettercollections.map.HashMap;
import de.blahord.predic.ast.Expression;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.typesystem.Type;
import de.blahord.predic.typesystem.generated.GeneratedType;
import de.blahord.predic.typesystem.generated.GeneratedTypeRegistry;
import de.blahord.predic.typesystem.generated.TypeGenerator;
import de.blahord.predic.typesystem.generated.TypeParameterDirection;

import java.util.function.BiFunction;
import java.util.function.Function;

import static com.github.blahord.bettercollections.util.Lists.emptyList;

public class ParserContext {

    private final String programCode;

    private final ExtensibleMap<
        ? extends Integer, ? extends TypeGenerator,
        ? super Integer, ? super TypeGenerator
        > functionTypeGenerators = HashMap.create();

    private final GeneratedTypeRegistry generatedTypeRegistry = new GeneratedTypeRegistry();

    private final MutableList<? extends ContextFrame, ? super ContextFrame> contextFrameStack = ArrayList.create();

    public ParserContext(String programCode) {
        this.programCode = programCode;
        contextFrameStack.add(new ContextFrame());
    }

    public String getProgramCode() {
        return programCode;
    }

    public TypeGenerator getTypeGenerator(Symbol classSymbol, int numberTypeParameters) {
        if (classSymbol.equals(Symbol.SYMBOL_FUNCTION)) {
            return getFunctionTypeGenerator(numberTypeParameters);
        }
        return find(classSymbol, ContextFrame::getTypeGenerator);
    }

    public void addTypeGenerator(Symbol classSymbol, List<? extends TypeParameterDirection> typeParameterDirections) {
        getCurrentContextFrame().addTypeGenerator(classSymbol, typeParameterDirections, generatedTypeRegistry);
    }

    public void addInheritance(Symbol subClassSymbol, Symbol superClassSymbol, int numberTypeParams, Function<? super List<? extends Type>, ? extends List<? extends Type>> mapping) {
        getTypeGenerator(subClassSymbol, 0).addInheritance(
            getTypeGenerator(superClassSymbol, numberTypeParams),
            mapping
        );
    }

    public Symbol getSymbol(String symbolName) {
        return find(symbolName, ContextFrame::getSymbol);
    }

    public Expression getExpression(Symbol symbol) {
        return find(symbol, ContextFrame::getExpression);
    }

    public void putExpression(Symbol symbol, Expression expression) {
        getCurrentContextFrame().addExpression(symbol, expression);
    }

    public Type generateType(Symbol classSymbol, List<? extends Type> typeParameter) {
        return Type.singletonType(generateGeneratedType(classSymbol, typeParameter));
    }

    public GeneratedType generateGeneratedType(Symbol classSymbol, List<? extends Type> typeParameter) {
        return getTypeGenerator(classSymbol, typeParameter.size()).generate(typeParameter);
    }

    public boolean isSymbolKnown(Symbol symbol) {
        return contextFrameStack.stream().anyMatch(c -> c.isSymbolKnown(symbol));
    }

    public void pushContextFrame() {
        contextFrameStack.add(new ContextFrame());
    }

    public void popContextFrame() {
        contextFrameStack.removeAt(contextFrameStack.size() - 1);
    }

    private TypeGenerator getFunctionTypeGenerator(int numberTypeParameters) {
        if (functionTypeGenerators.containsKey(numberTypeParameters)) {
            return functionTypeGenerators.get(numberTypeParameters);
        }

        final ExtensibleList<
            ? extends TypeParameterDirection,
            ? super TypeParameterDirection
            > typeParameterDirections = ArrayList.create();

        for (int i = 0; i < numberTypeParameters - 1; i++) {
            typeParameterDirections.add(TypeParameterDirection.IN);
        }
        typeParameterDirections.add(TypeParameterDirection.OUT);

        final TypeGenerator functionTypeGenerator = new TypeGenerator("function_" + numberTypeParameters, typeParameterDirections, generatedTypeRegistry);
        functionTypeGenerators.put(numberTypeParameters, functionTypeGenerator);
        functionTypeGenerator.addInheritance(getTypeGenerator(Symbol.SYMBOL_OBJECT, 0), l -> emptyList());
        return functionTypeGenerator;
    }

    private ContextFrame getCurrentContextFrame() {
        return contextFrameStack.at(contextFrameStack.size() - 1);
    }

    private <T, S> T find(S classSymbol, BiFunction<? super ContextFrame, ? super S, ? extends T> getterFromContext) {
        for (int i = contextFrameStack.size() - 1; i >= 0; i--) {
            if (getterFromContext.apply(contextFrameStack.at(i), classSymbol) != null) {
                return getterFromContext.apply(contextFrameStack.at(i), classSymbol);
            }
        }

        return null;
    }
}
