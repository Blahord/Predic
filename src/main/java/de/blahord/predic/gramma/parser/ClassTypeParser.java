package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.hasChildren;

public class ClassTypeParser {


    public static Type parseClassType(Tree<? extends SyntaxNode<PredicToken>> classTypeNode, ParserContext parserContext) {
        final Symbol classSymbol = SymbolParser.parseSymbol(getChild(classTypeNode, 0), parserContext, true);

        final ExtensibleList<? extends Type, ? super Type> typeParameters = ArrayList.create();
        final Tree<? extends SyntaxNode<PredicToken>> optTypeParameterNode = getChild(classTypeNode, 1);
        if (hasChildren(optTypeParameterNode)) {
            final Tree<? extends SyntaxNode<PredicToken>> typeParameterListNode = getChild(optTypeParameterNode, 0);
            typeParameters.add(TypeParser.parseType(getChild(typeParameterListNode, 1), parserContext));

            for (Tree<? extends SyntaxNode<PredicToken>> otherParameterNode : in(getChild(typeParameterListNode, 2).subTrees())) {
                typeParameters.add(TypeParser.parseType(getChild(otherParameterNode, 1), parserContext));
            }
        }

        return parserContext.generateType(classSymbol, typeParameters);
    }
}
