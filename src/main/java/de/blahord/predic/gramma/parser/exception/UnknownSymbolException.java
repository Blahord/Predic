package de.blahord.predic.gramma.parser.exception;

public class UnknownSymbolException extends RuntimeException {

    public UnknownSymbolException(String s) {
        super(s);
    }
}
