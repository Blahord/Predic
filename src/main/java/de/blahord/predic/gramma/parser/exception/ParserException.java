package de.blahord.predic.gramma.parser.exception;

public class ParserException extends RuntimeException {

    public ParserException(String s) {
        super(s);
    }
}
