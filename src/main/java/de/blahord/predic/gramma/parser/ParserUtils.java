package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.iterable.Iterable;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.lexiker.matcher.Range;
import de.blahord.predic.ast.ASTNode;
import de.blahord.predic.ast.CodeRange;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;

public class ParserUtils {

    public static Tree<? extends SyntaxNode<PredicToken>> getChild(Tree<? extends SyntaxNode<PredicToken>> node, List<? extends Integer> indexes) {
        if (indexes.isEmpty()) return node;
        else return getChild(getChild(node, indexes.at(0)), indexes.subList(1, indexes.size()));
    }

    public static Tree<? extends SyntaxNode<PredicToken>> getChild(Tree<? extends SyntaxNode<PredicToken>> node, int index) {
        return node.subTrees().at(index);
    }

    public static String getText(Tree<? extends SyntaxNode<PredicToken>> node, String text) {
        return node.item().getSubText();
    }

    public static boolean hasChildren(Tree<?> node) {
        return !node.subTrees().isEmpty();
    }

    public static CodeRange getCodeRange(SyntaxNode<?> syntaxNode) {
        final Range range = syntaxNode.getRange();

        return new CodeRange(range.getBeginLine(), range.getBeginColumn(),
                             range.getEndLine(), range.getEndColumn());
    }

    public static CodeRange joinCodeRange(CodeRange defaultCodeRange, Iterable<? extends ASTNode> astNodes) {
        CodeRange codeRange = defaultCodeRange;
        for (ASTNode node : in(astNodes)) {
            codeRange = codeRange.join(node.getCodeRange());
        }

        return codeRange;
    }
}
