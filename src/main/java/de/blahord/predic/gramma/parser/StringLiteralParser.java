package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.StringLiteral;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;

import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;
import static de.blahord.predic.gramma.parser.ParserUtils.getText;

public class StringLiteralParser {

    public static StringLiteral parseStringLiteral(Tree<? extends SyntaxNode<PredicToken>> stringLiteralNode, ParserContext parserContext) {
        final StringLiteral stringLiteral = new StringLiteral(getCodeRange(stringLiteralNode.item()), getText(stringLiteralNode, parserContext.getProgramCode()));
        stringLiteral.setType(parserContext.generateType(Symbol.SYMBOL_STRING, emptyList()));

        return stringLiteral;
    }
}
