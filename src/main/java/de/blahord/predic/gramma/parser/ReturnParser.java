package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Expression;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar;

public class ReturnParser {

    public static Expression parseReturn(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> returnTree, ParserContext parserContext) {

        return ExpressionParser.parseExpression(returnTree.subTrees().at(1), parserContext);
    }
}
