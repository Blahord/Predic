package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Literal;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.gramma.parser.exception.ParserException;

import static de.blahord.predic.gramma.parser.ParserUtils.getChild;

public class LiteralParser {

    public static Literal parseLiteral(Tree<? extends SyntaxNode<PredicToken>> literalNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicToken>> concreteLiteralNode = getChild(literalNode, 0);

        switch (concreteLiteralNode.item().getToken()) {
            case LITERAL_NUMBER:
                return NumberLiteralParser.parseNumberLiteral(concreteLiteralNode, parserContext);
            case LITERAL_STRING:
                return StringLiteralParser.parseStringLiteral(concreteLiteralNode, parserContext);
            case LITERAL_BOOLEAN:
                return BooleanLiteralParser.parseBooleanLiteral(concreteLiteralNode, parserContext);
            default:
                throw new ParserException("Unknown literal type: " + concreteLiteralNode.item().getToken());
        }

    }
}
