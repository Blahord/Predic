package de.blahord.predic.gramma.parser;

import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.Sets.emptySet;

public class NothingTypeParser {

    public static Type parseNothingType() {
        return new Type(emptySet());
    }
}
