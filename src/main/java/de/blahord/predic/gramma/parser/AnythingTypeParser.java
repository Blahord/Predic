package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.util.Sets;
import de.blahord.predic.typesystem.BaseType;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.Sets.singletonSet;

public class AnythingTypeParser {

    public static Type parseAnythingType() {
        return new Type(singletonSet(Sets.<BaseType>emptySet()));
    }
}
