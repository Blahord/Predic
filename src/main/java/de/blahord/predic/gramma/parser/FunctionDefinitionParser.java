package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.*;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;
import static de.blahord.predic.gramma.parser.ParserUtils.joinCodeRange;

public class FunctionDefinitionParser {

    public static FunctionDefinition parseFunctionDefinition(Tree<? extends SyntaxNode<PredicToken>> functionDefinitionNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicToken>> argumentsListNode = getChild(functionDefinitionNode, 1);

        final Tree<? extends SyntaxNode<PredicToken>> bodyNode = getChild(functionDefinitionNode, 3);


        final List<? extends Parameter> functionParameters = ParameterListParser.parseFunctionParameters(argumentsListNode, parserContext);
        final CodeRange codeRange = joinCodeRange(getCodeRange(bodyNode.item()), functionParameters);
        final FunctionDefinition functionDefinition = new FunctionDefinition(codeRange, functionParameters);

        parserContext.pushContextFrame();
        for (Parameter parameter : in(functionParameters)) {
            parserContext.putExpression(parameter.getSymbol(), null);
        }

        getBody(functionDefinitionNode, parserContext, functionDefinition);
        parserContext.popContextFrame();

        return functionDefinition;
    }

    private static void getBody(Tree<? extends SyntaxNode<PredicToken>> functionDefinitionNode, ParserContext parserContext, FunctionDefinition functionDefinition) {
        Type newType = functionDefinition.getType();
        Type oldType;
        Body body;

        do {
            parserContext.pushContextFrame();
            oldType = newType;
            functionDefinition.setReturnType(oldType, parserContext);
            parserContext.putExpression(new Symbol(CodeRange.EMPTY, "this"), functionDefinition);
            body = BodyParser.parseBody(getChild(functionDefinitionNode, 3), parserContext);
            newType = body.getExpression().getType();
            parserContext.popContextFrame();
        } while (!Type.isTypeEqual(oldType, newType));

        functionDefinition.setBody(body);
    }
}
