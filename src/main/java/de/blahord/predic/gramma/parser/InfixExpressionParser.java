package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Expression;
import de.blahord.predic.ast.ArithmeticExpression;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.gramma.parser.exception.WrongTypeException;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.getText;

public class InfixExpressionParser {

    public static Expression parseInfixExpression(Tree<? extends SyntaxNode<PredicToken>> ifExpressionNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicToken>> expr1Node = getChild(ifExpressionNode, 0);
        final Tree<? extends SyntaxNode<PredicToken>> operatorNode = getChild(ifExpressionNode, 1);
        final Tree<? extends SyntaxNode<PredicToken>> expr2Node = getChild(ifExpressionNode, 2);

        final Expression expr1 = ExpressionParser.parseExpression(expr1Node, parserContext);
        final String operator = operatorNode.item().getSubText();
        final Expression expr2 = ExpressionParser.parseExpression(expr2Node, parserContext);

        final Type expressionType = getExpressionType(operator, parserContext);
        final Type expectedType = parserContext.generateType(Symbol.SYMBOL_NUMBER, emptyList());

        if (!expr1.getType().isSubType(expectedType)) {
            throw new WrongTypeException(expr1.getType(), expressionType, getText(expr1Node, parserContext.getProgramCode()));
        }
        if (!expr2.getType().isSubType(expectedType)) {
            throw new WrongTypeException(expr2.getType(), expressionType, getText(expr1Node, parserContext.getProgramCode()));
        }

        final ArithmeticExpression arithmeticExpression = new ArithmeticExpression(expr1, operator, expr2);
        arithmeticExpression.setType(expressionType);
        return arithmeticExpression;
    }

    private static Type getExpressionType(String operator, ParserContext parserContext) {
        switch (operator) {
            case "+":
            case "-":
                return parserContext.generateType(Symbol.SYMBOL_NUMBER, emptyList());
            case "<":
            case "<=":
            case ">":
            case ">=":
            case "=":
                return parserContext.generateType(Symbol.SYMBOL_BOOLEAN, emptyList());
            default:
                return Type.anythingType();
        }
    }
}
