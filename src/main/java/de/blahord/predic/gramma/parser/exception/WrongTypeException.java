package de.blahord.predic.gramma.parser.exception;

import de.blahord.predic.typesystem.Type;

public class WrongTypeException extends RuntimeException {

    public WrongTypeException(Type actType, Type expType, String programPart) {
        super("Wrong type for\n" +
              "====\n" +
              programPart+"\n" +
              "====\n" +
              "Expected: "+expType+"\n" +
              "Got: "+actType);
    }
}
