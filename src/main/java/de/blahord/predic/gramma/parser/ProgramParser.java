package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Body;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;

import static de.blahord.predic.gramma.parser.ParserUtils.getChild;

public class ProgramParser {

    public static Body parseProgram(Tree<? extends SyntaxNode<PredicToken>> programNode, ParserContext parserContext) {
        return BodyParser.parseBody(getChild(programNode, 0), parserContext);
    }
}
