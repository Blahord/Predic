package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.*;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;

import static com.github.blahord.bettercollections.stream.Collectors.toArrayList;
import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;

public class BodyParser {

    public static Body parseBody(Tree<? extends SyntaxNode<PredicToken>> programNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicToken>> classesNode = getChild(programNode, 0);

        final List<? extends ClassDefinition> classes = classesNode.subTrees()
            .stream()
            .map(n -> ClassDefinitionParser.parseClass(n, parserContext))
            .collect(toArrayList());

        final Tree<? extends SyntaxNode<PredicToken>> assignmentsNode = getChild(programNode, 1);

        final List<? extends Assignment> assignments = assignmentsNode.subTrees()
            .stream()
            .map(n -> AssignmentParser.parseAssignment(n, parserContext))
            .collect(toArrayList());

        final Expression expression = ReturnParser.parseReturn(getChild(programNode, 2), parserContext);

        CodeRange codeRange = expression.getCodeRange();
        for (ASTNode classDef : in(classes)) {
            codeRange = codeRange.join(classDef.getCodeRange());
        }
        for (ASTNode classDef : in(assignments)) {
            codeRange = codeRange.join(classDef.getCodeRange());
        }

        return new Body(codeRange, classes, assignments, expression);
    }
}

