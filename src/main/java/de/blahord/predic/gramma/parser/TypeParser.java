package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.gramma.parser.exception.ParserException;
import de.blahord.predic.typesystem.Type;

import static de.blahord.predic.gramma.parser.ParserUtils.getChild;

public class TypeParser {

    public static Type parseType(Tree<? extends SyntaxNode<PredicToken>> typeNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicToken>> concreteTypeNode = getChild(typeNode, 0);

        switch (concreteTypeNode.item().getRuleName()) {
            case "anythingType":
                return AnythingTypeParser.parseAnythingType();
            case "nothingType":
                return NothingTypeParser.parseNothingType();
            case "classType":
                return ClassTypeParser.parseClassType(concreteTypeNode, parserContext);
            default:
                throw new ParserException("Unknwon type-type: " + concreteTypeNode.item().getRuleName());
        }
    }
}
