package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.*;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.joinCodeRange;

public class ClassDefinitionParser {

    public static ClassDefinition parseClass(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> classNode, ParserContext parserContext) {

        final Symbol name = SymbolParser.parseSymbol(getChild(classNode, 1), parserContext, false);
        final List<? extends Parameter> parameters = ParameterListParser.parseFunctionParameters(getChild(classNode, 2), parserContext);
        final ExtensibleList<? extends Member, ? super Member> members = ArrayList.create();

        parserContext.addTypeGenerator(name, emptyList());
        final FunctionDefinition constructor = new FunctionDefinition(null, parameters);
        constructor.setReturnType(parserContext.generateType(name, emptyList()), parserContext);
        parserContext.putExpression(name, constructor);

        parserContext.pushContextFrame();

        for (Parameter parameter : in(parameters)) {
            parserContext.putExpression(parameter.getSymbol(), null);
        }

        for (Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> memberDef : in(getChild(classNode, 4).subTrees())) {
            members.add(parseMemberDefinition(memberDef, parserContext));
        }
        parserContext.popContextFrame();

        final CodeRange codeRange = joinCodeRange(name.getCodeRange(), members);

        return new ClassDefinition(codeRange, name, parameters, members);
    }

    private static Member parseMemberDefinition(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> memberDef, ParserContext parserContext) {
        final Symbol name = SymbolParser.parseSymbol(getChild(memberDef, 1), parserContext, false);
        final Expression expression = ExpressionParser.parseExpression(getChild(memberDef, 3), parserContext);

        parserContext.putExpression(name, expression);

        return new Member(name, expression);
    }
}
