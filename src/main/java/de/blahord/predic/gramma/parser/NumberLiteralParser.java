package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.NumberLiteral;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import org.apache.commons.math3.fraction.BigFraction;

import java.math.BigInteger;

import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;

public class NumberLiteralParser {

    public static NumberLiteral parseNumberLiteral(Tree<? extends SyntaxNode<PredicToken>> numberLiteralNode, ParserContext parserContext) {
        boolean minusSign = numberLiteralNode.item().getSubText().startsWith("-");

        final String numberString = numberLiteralNode.item().getSubText().substring(minusSign ? 1 : 0);
        final String[] parts = numberString.split("\\.");
        final String preCommaString = parts[0];
        final String postCommaString = parts.length > 1 ? parts[1] : "";

        final BigInteger sign = minusSign ? BigInteger.ZERO.subtract(BigInteger.ONE) : BigInteger.ONE;
        final BigInteger enumerator = sign.multiply(new BigInteger(preCommaString + postCommaString));
        final BigInteger divisor = powerOfTen(postCommaString.length());

        final NumberLiteral numberLiteral = new NumberLiteral(getCodeRange(numberLiteralNode.item()), new BigFraction(enumerator, divisor));
        numberLiteral.setType(parserContext.generateType(Symbol.SYMBOL_NUMBER, emptyList()));
        return numberLiteral;
    }

    private static BigInteger powerOfTen(int n) {
        if (n == 0) return BigInteger.ONE;
        return BigInteger.TEN.multiply(powerOfTen(n - 1));
    }
}
