package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Assignment;
import de.blahord.predic.ast.Expression;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;

import static de.blahord.predic.gramma.parser.ParserUtils.getChild;

public class AssignmentParser {

    public static Assignment parseAssignment(Tree<? extends SyntaxNode<PredicToken>> assignmentNode, ParserContext parserContext) {
        final Symbol symbol = SymbolParser.parseSymbol(getChild(assignmentNode, 1), parserContext, false);
        final Expression expression = ExpressionParser.parseExpression(getChild(assignmentNode, 3), parserContext);

        parserContext.putExpression(symbol, expression);
        symbol.setType(expression.getType());

        return new Assignment(symbol, expression);
    }
}
