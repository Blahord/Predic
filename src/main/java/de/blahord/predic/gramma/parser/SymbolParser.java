package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.gramma.parser.exception.UnknownSymbolException;

import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;
import static de.blahord.predic.gramma.parser.ParserUtils.getText;

public class SymbolParser {

    public static Symbol parseSymbol(Tree<? extends SyntaxNode<PredicToken>> symbolNode, ParserContext parserContext, boolean checkExistence) {
        final String symbolName = getText(symbolNode, parserContext.getProgramCode());

        if (checkExistence) {
            final Symbol symbol = parserContext.getSymbol(symbolName);
            if (symbol == null) {
                throw new UnknownSymbolException("Unknown Symbol: " + symbolName);
            }
            return symbol;
        }

        return new Symbol(getCodeRange(symbolNode.item()), symbolName);
    }
}
