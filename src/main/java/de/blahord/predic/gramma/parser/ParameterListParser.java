package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Parameter;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static com.github.blahord.bettercollections.util.Lists.asList;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;

public class ParameterListParser {

    public static List<? extends Parameter> parseFunctionParameters(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> node, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> argumentsListNode = getChild(node, 1);

        final ExtensibleList<? extends Parameter, ? super Parameter> parameters = ArrayList.create();

        if (!argumentsListNode.subTrees().isEmpty()) {
            final Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> actualArgumentListNode = getChild(argumentsListNode, 0);
            final Type firstType = TypeParser.parseType(getChild(actualArgumentListNode, asList(0, 0)), parserContext);
            final Symbol firstSymbol = SymbolParser.parseSymbol(getChild(actualArgumentListNode, asList(0, 1)), parserContext, false);
            firstSymbol.setType(firstType);

            parameters.add(new Parameter(getCodeRange(actualArgumentListNode.item()), firstType, firstSymbol));

            for (Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> otherArgumentNode : in(getChild(actualArgumentListNode, 1).subTrees())) {
                final Type otherType = TypeParser.parseType(getChild(otherArgumentNode, asList(1, 0)), parserContext);
                final Symbol otherSymbol = SymbolParser.parseSymbol(getChild(otherArgumentNode, asList(1, 1)), parserContext, false);
                otherSymbol.setType(otherType);

                parameters.add(new Parameter(getCodeRange(otherArgumentNode.item()), otherType, otherSymbol));
            }
        }

        return parameters;
    }
}
