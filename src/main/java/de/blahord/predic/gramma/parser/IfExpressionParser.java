package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.Expression;
import de.blahord.predic.ast.IfExpression;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;
import de.blahord.predic.gramma.parser.exception.WrongTypeException;
import de.blahord.predic.typesystem.Type;

import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.getText;

public class IfExpressionParser {

    public static Expression parseIfExpression(Tree<? extends SyntaxNode<PredicToken>> ifExpressionNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicToken>> condNode = getChild(ifExpressionNode, 2);
        final Tree<? extends SyntaxNode<PredicToken>> thenNode = getChild(ifExpressionNode, 4);
        final Tree<? extends SyntaxNode<PredicToken>> elseNode = getChild(ifExpressionNode, 6);

        final Expression condExpr = ExpressionParser.parseExpression(condNode, parserContext);
        final Expression thenExpr = ExpressionParser.parseExpression(thenNode, parserContext);
        final Expression elseExpr = ExpressionParser.parseExpression(elseNode, parserContext);

        final Type type = Type.join(thenExpr.getType(), elseExpr.getType());

        if (!condExpr.getType().isSubType(parserContext.generateType(Symbol.SYMBOL_BOOLEAN, emptyList()))) {
            throw new WrongTypeException(condExpr.getType(), parserContext.generateType(Symbol.SYMBOL_BOOLEAN, emptyList()), getText(condNode, parserContext.getProgramCode()));
        }

        final IfExpression ifExpression = new IfExpression(condExpr, thenExpr, elseExpr);
        ifExpression.setType(type);
        return ifExpression;
    }
}
