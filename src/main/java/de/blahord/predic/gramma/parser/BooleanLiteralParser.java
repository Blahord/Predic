package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.BooleanLiteral;
import de.blahord.predic.ast.CodeRange;
import de.blahord.predic.ast.Symbol;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar.PredicToken;

import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;
import static de.blahord.predic.gramma.parser.ParserUtils.getText;

public class BooleanLiteralParser {

    public static BooleanLiteral parseBooleanLiteral(Tree<? extends SyntaxNode<PredicToken>> booleanLiteralNode, ParserContext parserContext) {
        final BooleanLiteral booleanLiteral = new BooleanLiteral(getCodeRange(booleanLiteralNode.item()), "true".equals(getText(booleanLiteralNode, parserContext.getProgramCode())));
        booleanLiteral.setType(parserContext.generateType(Symbol.SYMBOL_BOOLEAN, emptyList()));

        return booleanLiteral;
    }
}
