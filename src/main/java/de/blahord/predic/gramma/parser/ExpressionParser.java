package de.blahord.predic.gramma.parser;

import com.github.blahord.bettercollections.list.ArrayList;
import com.github.blahord.bettercollections.list.ExtensibleList;
import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.set.Set;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.*;
import de.blahord.predic.gramma.ParserContext;
import de.blahord.predic.gramma.PredicGrammar;
import de.blahord.predic.gramma.parser.exception.ParserException;
import de.blahord.predic.gramma.parser.exception.WrongTypeException;
import de.blahord.predic.typesystem.Type;
import org.apache.commons.math3.fraction.BigFraction;

import java.util.stream.Stream;

import static com.github.blahord.bettercollections.stream.Collectors.toArrayList;
import static com.github.blahord.bettercollections.util.JavaForEachLoopIterable.in;
import static com.github.blahord.bettercollections.util.Lists.asList;
import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static com.github.blahord.bettercollections.util.Sets.asSet;
import static de.blahord.predic.gramma.parser.ParserUtils.getChild;
import static de.blahord.predic.gramma.parser.ParserUtils.getCodeRange;

public class ExpressionParser {

    public static Expression parseExpression(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> expressionNode, ParserContext parserContext) {
        final List<? extends ExpressionToken> tokens = parseTokens(expressionNode, parserContext);

        return buildExpression(tokens, parserContext);
    }

    private static List<? extends ExpressionToken> parseTokens(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> expressionNode, ParserContext parserContext) {
        return expressionNode.subTrees().stream().map(etn -> parseExpressionToken(etn, parserContext)).collect(toArrayList());
    }

    private static ExpressionToken parseExpressionToken(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> expressionTokenNode, ParserContext parserContext) {
        final Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> concreteExpressionNode = getChild(expressionTokenNode, 0);

        final CodeRange codeRange = getCodeRange(concreteExpressionNode.item());

        switch (concreteExpressionNode.item().getRuleName()) {
            case "literal":
                return new ExpressionToken(codeRange, LiteralParser.parseLiteral(concreteExpressionNode, parserContext));
            case "braceExpression":
                return new ExpressionToken(codeRange, parseExpressionList(concreteExpressionNode, parserContext));
            case "functionDefinition":
                return new ExpressionToken(codeRange, FunctionDefinitionParser.parseFunctionDefinition(concreteExpressionNode, parserContext));
            case "SYMBOL":
                return new ExpressionToken(codeRange, SymbolParser.parseSymbol(concreteExpressionNode, parserContext, false));
            case "OPERATOR":
                return new ExpressionToken(codeRange, Operator.findBySymbol(concreteExpressionNode.item().getSubText()));
            default:
                throw new ParserException("Unknown expression type: " + concreteExpressionNode.item().getRuleName());
        }
    }

    private static List<? extends Expression> parseExpressionList(Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> braceExpressionNode, ParserContext parserContext) {

        final Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> optionalFirstExpressionNode = getChild(braceExpressionNode, 1);
        if (optionalFirstExpressionNode.subTrees().isEmpty()) {
            return emptyList();
        }

        final Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> firstExpressionNode = getChild(optionalFirstExpressionNode, asList(0, 0));
        final Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> otherExpressionsNode = getChild(optionalFirstExpressionNode, asList(0, 1));

        final ExtensibleList<? extends Expression, ? super Expression> expressionList = ArrayList.create();
        expressionList.add(parseExpression(firstExpressionNode, parserContext));

        otherExpressionsNode.subTrees().forEach(
            oen -> expressionList.add(parseExpression(getChild(oen, 1), parserContext))
        );

        return expressionList;
    }

    private static Expression buildExpression(List<? extends ExpressionToken> tokens, ParserContext parserContext) {
        final Type numberType = parserContext.generateType(Symbol.SYMBOL_NUMBER, emptyList());
        final Type booleanType = parserContext.generateType(Symbol.SYMBOL_BOOLEAN, emptyList());

        if (listContainsOperator(tokens, Operator.IF_THEN)) {
            return buildIfThenElseExpression(tokens, parserContext);
//        } else if (listContainsOperator(tokens, Operator.OR)) {
//            return buildOrExpression(tokens, parserContext);
//        } else if (listContainsOperator(tokens, Operator.AND)) {
//            return buildAndExpression(tokens, parserContext);
        } else if (listContainsOperator(tokens, Operator.EQUAL)) {
            return buildArithmeticExpression(tokens, parserContext, numberType, booleanType, Operator.EQUAL);
        } else if (listContainsOneOperator(tokens, Operator.GREATER, Operator.GREATER_OR_EQUAL, Operator.SMALLER, Operator.SMALLER_OR_EQUAL)) {
            return buildArithmeticExpression(tokens, parserContext, numberType, booleanType, Operator.GREATER, Operator.GREATER_OR_EQUAL, Operator.SMALLER, Operator.SMALLER_OR_EQUAL);
        } else if (listContainsOneOperator(tokens, Operator.ADD, Operator.SUBTRACT)) {
            return buildArithmeticExpression(tokens, parserContext, numberType, numberType, Operator.ADD, Operator.SUBTRACT);
        } else if (listContainsOneOperator(tokens, Operator.MULTIPLY, Operator.DIVIDE)) {
            return buildArithmeticExpression(tokens, parserContext, numberType, numberType, Operator.MULTIPLY, Operator.DIVIDE);
        } else if (listContainsOperator(tokens, Operator.DOT) && maxIndexWithOperator(tokens, Operator.DOT) == tokens.size() - 2) {
            return buildFieldExpression(tokens, parserContext);
        } else if (listContainsBraceExpression(tokens)) {
            return buildBraceExpression(tokens, parserContext);
        }

        return checkExpression(tokens.at(0), parserContext);
    }

    private static Expression buildArithmeticExpression(List<? extends ExpressionToken> tokens, ParserContext parserContext, Type argumentsType, Type expressionType, Operator... operators) {
        final int operatorIndex = maxIndexWithOperator(tokens, operators);

        final List<? extends ExpressionToken> leftTokens = tokens.subList(0, operatorIndex);
        final List<? extends ExpressionToken> rightTokens = tokens.subList(operatorIndex + 1, tokens.size());

        final Expression leftExpression = leftTokens.isEmpty() ? new NumberLiteral(CodeRange.EMPTY, BigFraction.ZERO) : buildExpression(leftTokens, parserContext);
        final Expression rightExpression = buildExpression(rightTokens, parserContext);

        if (!leftExpression.getType().isSubType(argumentsType)) {
            throw new WrongTypeException(leftExpression.getType(), argumentsType, "");
        }
        if (!rightExpression.getType().isSubType(argumentsType)) {
            throw new WrongTypeException(leftExpression.getType(), argumentsType, "");
        }

        final Expression expression = new ArithmeticExpression(leftExpression, tokens.at(operatorIndex).operator.symbol, rightExpression);
        expression.setType(expressionType);
        return expression;
    }

    private static Expression buildBraceExpression(List<? extends ExpressionToken> tokens, ParserContext parserContext) {
        final int braceExpressionTokenIndex = maxIndexOfBraceExpression(tokens);

        if (braceExpressionTokenIndex == 0) {
            return tokens.at(0).bracedExpressionList.at(0);
        }


        final List<? extends ExpressionToken> functionExpressionTokens = tokens.subList(0, braceExpressionTokenIndex);
        final Expression functionExpression = buildExpression(functionExpressionTokens, parserContext);
        CodeRange codeRange = functionExpression.getCodeRange();
        List<? extends Expression> bracedExpressionList = tokens.at(braceExpressionTokenIndex).bracedExpressionList;
        for (Expression expression : in(bracedExpressionList)) {
            codeRange = codeRange.join(expression.getCodeRange());
        }

        return new FunctionCall(codeRange, functionExpression, bracedExpressionList, parserContext);
    }

    private static Expression buildFieldExpression(List<? extends ExpressionToken> tokens, ParserContext parserContext) {
        final int dotIndex = maxIndexOfOperator(tokens, Operator.DOT);

        final List<? extends ExpressionToken> leftTokens = tokens.subList(0, dotIndex);
        final ExpressionToken fieldToken = tokens.at(dotIndex + 1);

        final Expression leftExpression = buildExpression(leftTokens, parserContext);
        final Symbol access = (Symbol) fieldToken.expression;
        return new FieldAccessExpression(leftExpression, access);
    }

    private static Expression checkExpression(ExpressionToken token, ParserContext parserContext) {
        return token.expression;
    }

    private static Expression buildIfThenElseExpression(List<? extends ExpressionToken> tokens, ParserContext parserContext) {
        final int ifThenOperatorIndex = minIndexOfOperator(tokens, Operator.IF_THEN);
        final int ifElseOperatorIndex = maxIndexOfOperator(tokens, Operator.IF_ELSE);

        final List<? extends ExpressionToken> testExpressionTokenList = tokens.subList(0, ifThenOperatorIndex);
        final List<? extends ExpressionToken> thenExpressionTokenList = tokens.subList(ifThenOperatorIndex + 1, ifElseOperatorIndex);
        final List<? extends ExpressionToken> elseExpressionTokenList = tokens.subList(ifElseOperatorIndex + 1, tokens.size());


        Expression testExpression = buildExpression(testExpressionTokenList, parserContext);
        Expression thenExpression = buildExpression(thenExpressionTokenList, parserContext);
        Expression elseExpression = buildExpression(elseExpressionTokenList, parserContext);

        return new IfExpression(testExpression, thenExpression, elseExpression);
    }

    private static boolean listContainsOperator(List<? extends ExpressionToken> tokens, Operator operator) {
        return tokens.stream().anyMatch(t -> t.operator == operator);
    }

    private static boolean listContainsOneOperator(List<? extends ExpressionToken> tokens, Operator... operators) {
        final Set<? extends Operator> operatorSet = asSet(operators);
        return tokens.stream().anyMatch(t -> operatorSet.contains(t.operator));
    }

    private static boolean listContainsBraceExpression(List<? extends ExpressionToken> tokens) {
        return tokens.stream().anyMatch(t -> t.bracedExpressionList != null);
    }

    private static int minIndexOfOperator(List<? extends ExpressionToken> tokens, Operator operator) {
        for (int i = 0; i < tokens.size(); i++) {
            if (tokens.at(i).operator == operator) {
                return i;
            }
        }

        throw new ParserException("Expression List does not contain wanted operator");
    }

    private static int maxIndexOfOperator(List<? extends ExpressionToken> tokens, Operator operator) {
        for (int i = tokens.size() - 1; i >= 0; i--) {
            if (tokens.at(i).operator == operator) {
                return i;
            }
        }

        throw new ParserException("Expression List does not contain wanted operator");
    }

    private static int maxIndexWithOperator(List<? extends ExpressionToken> tokens, Operator... operators) {
        final Set<? extends Operator> operatorSet = asSet(operators);

        for (int i = tokens.size() - 1; i >= 0; i--) {
            if (operatorSet.contains(tokens.at(i).operator)) {
                return i;
            }
        }

        throw new ParserException("Expression List does not contain wanted operator");
    }

    private static int maxIndexOfBraceExpression(List<? extends ExpressionToken> tokens) {
        for (int i = tokens.size() - 1; i >= 0; i--) {
            if (tokens.at(i).bracedExpressionList != null) {
                return i;
            }
        }

        throw new ParserException("Expression List does not contain braces");
    }

    private static class ExpressionToken {

        private final CodeRange codeRange;

        protected final Expression expression;
        protected final Operator operator;
        protected final List<? extends Expression> bracedExpressionList;

        private ExpressionToken(CodeRange codeRange, Expression expression) {
            this(codeRange, expression, null, null);
        }

        private ExpressionToken(CodeRange codeRange, Operator operator) {
            this(codeRange, null, operator, null);
        }

        private ExpressionToken(CodeRange codeRange, List<? extends Expression> bracedExpressionList) {
            this(codeRange, null, null, bracedExpressionList);
        }

        private ExpressionToken(CodeRange codeRange, Expression expression, Operator operator, List<? extends Expression> bracedExpressionList) {
            this.codeRange = codeRange;
            this.expression = expression;
            this.operator = operator;
            this.bracedExpressionList = bracedExpressionList;
        }
    }

    private enum Operator {
        ADD("+"),
        SUBTRACT("-"),
        MULTIPLY("*"),
        DIVIDE("/"),
        DOT("."),
        IF_THEN("?"),
        IF_ELSE(":"),
        EQUAL("="),
        SMALLER("<"),
        SMALLER_OR_EQUAL("<="),
        GREATER(">"),
        GREATER_OR_EQUAL(">="),
        AND("&"),
        OR("|");

        private final String symbol;

        Operator(String symbol) {
            this.symbol = symbol;
        }

        static Operator findBySymbol(String symbol) {
            return Stream.of(values()).filter(op -> op.symbol.equals(symbol)).findFirst().orElse(null);
        }
    }
}
