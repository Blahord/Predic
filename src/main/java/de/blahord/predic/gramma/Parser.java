package de.blahord.predic.gramma;

import com.github.blahord.bettercollections.list.List;
import com.github.blahord.bettercollections.tree.Tree;
import de.blahord.lexiker.grammar.ParseResult;
import de.blahord.lexiker.grammar.SyntaxNode;
import de.blahord.predic.ast.*;
import de.blahord.predic.gramma.parser.ProgramParser;
import de.blahord.predic.typesystem.Type;
import de.blahord.predic.typesystem.generated.TypeParameterDirection;

import java.util.function.Function;

import static com.github.blahord.bettercollections.util.Lists.emptyList;
import static java.util.function.Function.identity;

public class Parser {

    private ParserContext parserContext;
    private EvalContext evalContext;

    public Parser(String code) {
        this.parserContext = new ParserContext(code);
        this.evalContext = new EvalContext();

        initParserContext();
    }

    public Parser addTypeGenerator(String name, List<? extends TypeParameterDirection> typeParameterDirections) {
        parserContext.addTypeGenerator(new Symbol(CodeRange.EMPTY, name), typeParameterDirections);

        return this;
    }

    public Parser addInheritance(String subType, String superType, Function<? super List<? extends Type>, ? extends List<? extends Type>> inheritanceFunction) {
        parserContext.addInheritance(new Symbol(CodeRange.EMPTY, subType), new Symbol(null, subType), 0, inheritanceFunction);

        return this;
    }

    public Type generateType(String typeName, List<? extends Type> typeParameters) {
        return parserContext.generateType(new Symbol(CodeRange.EMPTY, typeName), typeParameters);
    }

    public Object run() {
        ParseResult<PredicGrammar.PredicToken> parseResult = PredicGrammar.predicGramma.parse(parserContext.getProgramCode());
        Tree<? extends SyntaxNode<PredicGrammar.PredicToken>> ast = parseResult.getSyntaxTree();

        final Body body = ProgramParser.parseProgram(ast, parserContext);
        return body.evaluate(evalContext);
    }


    private void initParserContext() {
        parserContext.addTypeGenerator(Symbol.SYMBOL_OBJECT, emptyList());
        parserContext.addTypeGenerator(Symbol.SYMBOL_BOOLEAN, emptyList());
        parserContext.addTypeGenerator(Symbol.SYMBOL_NUMBER, emptyList());
        parserContext.addTypeGenerator(Symbol.SYMBOL_STRING, emptyList());

        parserContext.addInheritance(Symbol.SYMBOL_STRING, Symbol.SYMBOL_OBJECT, 0, identity());
        parserContext.addInheritance(Symbol.SYMBOL_NUMBER, Symbol.SYMBOL_OBJECT, 0, identity());
        parserContext.addInheritance(Symbol.SYMBOL_BOOLEAN, Symbol.SYMBOL_OBJECT, 0, identity());

        parserContext.putExpression(Symbol.SYMBOL_OBJECT, new ClassExpression(CodeRange.EMPTY, "Object"));
        parserContext.putExpression(Symbol.SYMBOL_NUMBER, new ClassExpression(CodeRange.EMPTY, "Number"));
        parserContext.putExpression(Symbol.SYMBOL_STRING, new ClassExpression(CodeRange.EMPTY, "String"));
        parserContext.putExpression(Symbol.SYMBOL_BOOLEAN, new ClassExpression(CodeRange.EMPTY, "Boolean"));
        parserContext.putExpression(Symbol.SYMBOL_FUNCTION, new ClassExpression(CodeRange.EMPTY, "Function"));
    }
}
