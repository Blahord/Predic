package de.blahord.predic.gramma;

import de.blahord.lexiker.grammar.Grammar;

import static de.blahord.lexiker.grammar.Grammar.*;

public class PredicGrammar {

    public enum PredicToken {
        SYMBOL,
        CLASS,
        LET,
        DEF,
        RETURN,
        OP_ASSIGN,
        CLASS_NAME,
        TYPE_PARAM_BRACE_OPEN,
        TYPE_PARAM_BRACE_CLOSE,
        TYPE_PARAM_COMMA,
        TYPE_ANYTHING,
        TYPE_NOTHING,
        FUNCTION_IF,
        FUNCTIONDEF,
        FUNCTIONDEF_PARAM_BRACE_OPEN,
        FUNCTIONDEF_PARAM_BRACE_CLOSE,
        FUNCTIONDEF_PARAM_COMMA,
        FUNCTIONDEF_BODYBRACE_OPEN,
        FUNCTIONDEF_BODYBRACE_CLOSE,
        FUNCTION_CALL_PARAM_BRACE_OPEN,
        FUNCTION_CALL_PARAM_BRACE_CLOSE,
        FUNCTION_CALL_PARAM_COMMA,
        LITERAL_STRING,
        LITERAL_NUMBER,
        LITERAL_BOOLEAN,
        LOAD_FNCTION_PREFIX,
        SEMICOLON,
        OPERATOR,
        EXPRESSION_BRACE_OPEN,
        EXPRESSION_BRACE_CLOSE,
    }

    public static final Grammar<PredicToken> predicGramma = new Grammar<>(PredicToken.class);

    static {
        predicGramma.whitespace("([ \t\n]|(//[^\n]*(\n|\\z)))*");

        predicGramma.token(PredicToken.CLASS, "class");
        predicGramma.token(PredicToken.LET, "let");
        predicGramma.token(PredicToken.DEF, "def");
        predicGramma.token(PredicToken.RETURN, "return");
        predicGramma.token(PredicToken.SYMBOL, "[a-zA-Z]\\w*");
        predicGramma.token(PredicToken.OP_ASSIGN, "\\=");
        predicGramma.token(PredicToken.CLASS_NAME, "[a-zA-Z]\\w*");
        predicGramma.token(PredicToken.TYPE_PARAM_BRACE_OPEN, "\\<");
        predicGramma.token(PredicToken.TYPE_PARAM_BRACE_CLOSE, "\\>");
        predicGramma.token(PredicToken.TYPE_PARAM_COMMA, ",");
        predicGramma.token(PredicToken.TYPE_ANYTHING, "anything");
        predicGramma.token(PredicToken.TYPE_NOTHING, "nothing");
        predicGramma.token(PredicToken.FUNCTION_IF, "if");
        predicGramma.token(PredicToken.FUNCTIONDEF, "function");
        predicGramma.token(PredicToken.FUNCTIONDEF_PARAM_BRACE_OPEN, "\\(");
        predicGramma.token(PredicToken.FUNCTIONDEF_PARAM_BRACE_CLOSE, "\\)");
        predicGramma.token(PredicToken.FUNCTIONDEF_PARAM_COMMA, ",");
        predicGramma.token(PredicToken.FUNCTIONDEF_BODYBRACE_OPEN, "\\{");
        predicGramma.token(PredicToken.FUNCTIONDEF_BODYBRACE_CLOSE, "\\}");
        predicGramma.token(PredicToken.FUNCTION_CALL_PARAM_BRACE_OPEN, "\\(");
        predicGramma.token(PredicToken.FUNCTION_CALL_PARAM_BRACE_CLOSE, "\\)");
        predicGramma.token(PredicToken.FUNCTION_CALL_PARAM_COMMA, ",");
        predicGramma.token(PredicToken.LITERAL_STRING, "\"[^\"]*\"");
        predicGramma.token(PredicToken.LITERAL_NUMBER, "-?[0-9]+(\\.[0-9]+)?");
        predicGramma.token(PredicToken.LITERAL_BOOLEAN, "(true)|(false)");
        predicGramma.token(PredicToken.LOAD_FNCTION_PREFIX, "\\@");
        predicGramma.token(PredicToken.SEMICOLON, ";");
        predicGramma.token(PredicToken.OPERATOR, "[+\\-*/?:=\\.]|([<>](=)?)");
        predicGramma.token(PredicToken.EXPRESSION_BRACE_OPEN, "\\(");
        predicGramma.token(PredicToken.EXPRESSION_BRACE_CLOSE, "\\)");

        predicGramma.defineRule(
            "program",
            sequence(
                ref("body"),
                eoi()
            )
        );

        predicGramma.defineRule(
            "body",
            sequence(
                ref("classList"),
                ref("assignmentList"),
                ref("return"),
                token(PredicToken.SEMICOLON)
            )
        );

        predicGramma.defineRule(
            "classList",
            repeat(
                ref("classDefinition"), 0, null
            )
        );

        predicGramma.defineRule(
            "classDefinition",
            sequence(
                token(PredicToken.CLASS),
                token(PredicToken.SYMBOL),
                ref("parameterList"),
                token(PredicToken.FUNCTIONDEF_BODYBRACE_OPEN),
                repeat(
                    ref("memberDefinition"), 0, null
                ),
                token(PredicToken.FUNCTIONDEF_BODYBRACE_CLOSE),
                token(PredicToken.SEMICOLON)
            )
        );

        predicGramma.defineRule(
            "memberDefinition",
            sequence(
                token(PredicToken.DEF),
                token(PredicToken.SYMBOL),
                token(PredicToken.OP_ASSIGN),
                ref("expression"),
                token(PredicToken.SEMICOLON)
            )
        );

        predicGramma.defineRule(
            "return",
            sequence(
                token(PredicToken.RETURN),
                ref("expression")
            )
        );

        predicGramma.defineRule(
            "assignmentList",
            repeat(
                ref("assignment"), 0, null
            )
        );

        predicGramma.defineRule(
            "type",
            firstOf(
                token(PredicToken.TYPE_ANYTHING),
                token(PredicToken.TYPE_NOTHING),
                ref("classType")
            )
        );

        predicGramma.defineRule(
            "classType",
            sequence(
                token(PredicToken.CLASS_NAME),
                repeat(
                    sequence(
                        token(PredicToken.TYPE_PARAM_BRACE_OPEN),
                        ref("type"),
                        repeat(
                            sequence(
                                token(PredicToken.TYPE_PARAM_COMMA),
                                ref("type")
                            ),
                            0, null
                        ),
                        token(PredicToken.TYPE_PARAM_BRACE_CLOSE)
                    ),
                    0, 1
                )
            )
        );

        predicGramma.defineRule(
            "expression",
            repeat(
                ref("expressionToken"),
                1, null
            )
        );

        predicGramma.defineRule(
            "expressionToken",
            firstOf(
                token(PredicToken.OPERATOR),
                ref("literal"),
                ref("functionDefinition"),
                ref("braceExpression"),
                token(PredicToken.SYMBOL)
            )
        );

        predicGramma.defineRule(
            "braceExpression",
            sequence(
                token(PredicToken.EXPRESSION_BRACE_OPEN),
                ref("expressionList"),
                token(PredicToken.EXPRESSION_BRACE_CLOSE)
            )
        );

        predicGramma.defineRule(
            "expressionList",
            repeat(
                sequence(
                    ref("expression"),
                    repeat(
                        sequence(
                            token(PredicToken.FUNCTION_CALL_PARAM_COMMA),
                            ref("expression")
                        ),
                        0, null
                    )
                ),
                0, 1
            )
        );
        predicGramma.defineRule(
            "assignment",
            sequence(
                token(PredicToken.LET),
                token(PredicToken.SYMBOL),
                token(PredicToken.OP_ASSIGN),
                ref("expression"),
                token(PredicToken.SEMICOLON)
            )
        );

        predicGramma.defineRule(
            "functionDefinition",
            sequence(
                token(PredicToken.FUNCTIONDEF),
                ref("parameterList"),
                token(PredicToken.FUNCTIONDEF_BODYBRACE_OPEN),
                ref("body"),
                token(PredicToken.FUNCTIONDEF_BODYBRACE_CLOSE)
            )
        );

        predicGramma.defineRule(
            "parameterList",
            sequence(
                token(PredicToken.FUNCTIONDEF_PARAM_BRACE_OPEN),
                repeat(
                    sequence(
                        ref("functionParam"),
                        repeat(
                            sequence(
                                token(PredicToken.FUNCTIONDEF_PARAM_COMMA),
                                ref("functionParam")
                            ),
                            0, null
                        )
                    ),
                    0, 1
                ),
                token(PredicToken.FUNCTIONDEF_PARAM_BRACE_CLOSE)
            )
        );

        predicGramma.defineRule(
            "functionParam",
            sequence(
                ref("type"),
                token(PredicToken.SYMBOL)
            )
        );
        predicGramma.defineRule(
            "literal",
            firstOf(
                token(PredicToken.LITERAL_NUMBER),
                token(PredicToken.LITERAL_STRING),
                token(PredicToken.LITERAL_BOOLEAN)
            )
        );

        predicGramma.setMainRule("program");
    }
}
